## Backup encryption keys

```
kubectl get secret -n kube-system -l sealedsecrets.bitnami.com/sealed-secrets-key -o yaml > master.key
```

## Backup encryption keys

To restore from a backup after some disaster, just put that secrets back before
starting the controller - or if the controller was already started, replace the
newly-created secrets and restart the controller:

```
kubectl apply -f master.key
kubectl delete pod -n kube-system -l name=sealed-secrets-controller
```

